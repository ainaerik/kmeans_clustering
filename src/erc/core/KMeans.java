package erc.core;

import erc.libs.Helpers;

import java.util.ArrayList;

/**
 * Ny Aina Erick on Thu Nov 1, 2018 11:51 AM
 * */
public class KMeans {

    private int cluster;
    private ArrayList<Point> centroids;
    private ArrayList<Point> data;

    public KMeans(int cluster, ArrayList<Point> data) {
        this(cluster, data, null);
    }

    public KMeans(int cluster, ArrayList<Point> data, ArrayList<Point> centroids) {
        this.cluster = cluster;
        this.centroids = centroids;
        this.data = data;
    }

    /**
     * compute kmeans algorithm
     * */
    public ArrayList<ArrayList<Point>> run() {
        if (this.centroids == null) generateCentroids();

        ArrayList<ArrayList<Point>> clusters;
        ArrayList<ArrayList<Point>> currentClusters;
        ArrayList<Point> newCentroids;

        clusters = this.compute();

        while(true) {
            newCentroids = updateCentroids(clusters);
            currentClusters = this.compute();

            if (currentClusters.equals(clusters)) break;
            else clusters = currentClusters;
        }

        setCentroids(newCentroids);
        return clusters;
    }

    /**
     * Compute clustering
     * */
    public ArrayList<ArrayList<Point>> compute() {
        double dist;

        ArrayList<ArrayList<Point>> clusters = new ArrayList<>();
        for (int i = 0; i < this.getCluster(); i++) clusters.add(new ArrayList<>());

        for (int i = 0; i < this.getData().size(); i++) {
            double min = Double.MAX_VALUE;
            int c = 0;
            for (int j = 0; j < this.getCentroids().size(); j++) {
                dist = euclideanDistance(this.data.get(i), this.centroids.get(j));
                if (min > dist) {
                    min = dist;
                    c = j;
                }
            }
            clusters.get(c).add(this.data.get(i));
        }
        return clusters;
    }

    /**
     * Update the value of centroid with new cluster
     * @param points
     * */
    private ArrayList<Point> updateCentroids(ArrayList<ArrayList<Point>> points) {
        ArrayList<Point> centroids = new ArrayList<>();
        int i = 0;
        double sumX, sumY;

        for (ArrayList<Point> cluster: points) {
            sumX = 0;
            sumY = 0;
            for (Point point: cluster) {
                sumX += point.getX();
                sumY += point.getY();
            }
            centroids.add(new Point("C" + (++i), sumX / cluster.size(), sumY / cluster.size()));
        }
        setCentroids(centroids);
        return centroids;
    }

    /**
     * Generate initial centroids with random values
     * */
    private void generateCentroids() {
        ArrayList<Point> centroids = new ArrayList<>();

        double maxX = Helpers.maxOnAxis("X", data);
        double maxY = Helpers.maxOnAxis("Y", data);
        double x;
        double y;

        for (int i = 0; i < this.getCluster(); i++) {
            x = Math.random() * maxX;
            y = Math.random() * maxY;
            centroids.add(new Point("C"+(i+1), x, y));
        }

        this.setCentroids(centroids);
    }

    /**
     * Compute Euclidean distance between two points
     * @param p1
     * @param p2
     * */
    private double euclideanDistance(Point p1, Point p2) {
        double dist = Math.pow((p1.getX() - p2.getX()), 2) + Math.pow((p1.getY() - p2.getY()), 2);
        return Math.sqrt(dist);
    }

    public void setData(ArrayList<Point> data) {
        this.data = data;
    }

    public ArrayList<Point> getData() {
        return this.data;
    }

    public void setCentroids(ArrayList<Point> centroids) {
        this.centroids = centroids;
    }

    public ArrayList<Point> getCentroids() {
        return this.centroids;
    }

    public void setCluster(int cluster) {
        this.cluster = cluster;
    }

    public int getCluster() {
        return this.cluster;
    }

}