package erc.libs;

import erc.core.Point;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Ny Aina Erick on Sat Nov 3, 2018 10:43 AM
 * */
public final class Helpers {

    /**
     * Load data sets from csv file
     * @param path
     * */
    public static ArrayList<Point> loadData(String path) {
        BufferedReader bufferedReader;
        String line = null;
        ArrayList<Point> points = new ArrayList<>();

        try {
            bufferedReader = new BufferedReader(new FileReader(path));
            while ((line = bufferedReader.readLine()) != null) {
                points.add(parseLine(line));
            }
        }
        catch (FileNotFoundException e) {
            Logger.getLogger(e.getMessage());
        } catch (IOException e) {
            Logger.getLogger(e.getMessage());
        }

        return points;
    }

    /**
     * Parse comma separated line into Point class
     * @param line
     * */
    public static Point parseLine(String line) {
        String[] lineTab = line.split(";");
        if (lineTab.length > 3) return null;

        Point p = new Point();
        try {
            p.setLabel(lineTab[0]);
            p.setX(Double.parseDouble(lineTab[1]));
            p.setY(Double.parseDouble(lineTab[2]));
        }
        catch (NumberFormatException e) {
            Logger.getLogger(e.getMessage());
        }
        return p;
    }

    /**
     * Get maximum value on axis (X axis or Y axis)
     * @param axis
     * @param points
     * */
    public static double maxOnAxis(String axis, ArrayList<Point> points) {
        double maxValue = Double.MIN_VALUE;

        if (axis.equals("X"))
            for (int i = 0; i < points.size(); i++)
                if (maxValue < points.get(i).getX()) maxValue = points.get(i).getX();

        if (axis.equals("Y"))
            for (int j = 0; j < points.size(); j++)
                if (maxValue < points.get(j).getY()) maxValue = points.get(j).getY();

        return maxValue;
    }

    /**
     * Get minimum value on params
     * @param values
     * */
    public static double min(double... values) {
        double minValue = Double.MAX_VALUE;

        for (double value: values)
            if (minValue > value) minValue = value;
        return minValue;
    }

}
